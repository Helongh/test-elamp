import React from 'react'
import ReactDOM from 'react-dom'
import Promise from 'promise-polyfill'

import App from './components/App'


ReactDOM.render(<App />, document.getElementById('root'));
