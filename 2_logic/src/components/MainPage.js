import React, { Component } from 'react'
import MainContentBlock from "./MainCenterBlock";
import { Route } from 'react-router-dom'
import RightDetailPane from "./RightDetailPane";

class MainPage extends Component {
    render() {
        return (
            <div>
                <MainContentBlock/>
                <Route path="/:id" component={RightDetailPane}/>
            </div>
        )
    }
}

export default MainPage
