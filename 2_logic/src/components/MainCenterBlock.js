import React, { Component } from 'react'
import { listItems } from '../models/api'
import { Link } from 'react-router-dom'


class MainContentBlock extends Component {
    state = {
        items: [],
        loading: false
    }

    async componentDidMount() {
        this.setState({loading: true})
        const items = await listItems()
        this.setState({
            items: items.data
        })
        this.setState({loading: false})

    }

    renderLinks = items => {
        console.log(items)
        return (
            <ul>
                {
                    items.map(item => (
                        <li key={item.id}>
                            <Link to={`/${item.id}`}>{item.name}</Link>
                        </li>
                    ))
                }
            </ul>
        )
    }

    render() {
        return (
            this.state.loading ? 'Loading' : this.renderLinks(this.state.items)
        )
    }

}

export default MainContentBlock
