const DATA = require('./data.json')

function listHtml(children) {
    return  '<ul>' + children.map(node =>
        '<li>' + node.name +
        (node.children ? listHtml(node.children) : '') +
        '</li>').join('\n') +
        '</ul>';
}

const html = listHtml(DATA)

console.log(html)
