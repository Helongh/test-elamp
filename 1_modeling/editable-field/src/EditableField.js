import React, { Component } from 'react'

class EditableField extends Component {
    state = {
        value: "SomeValue",
        editing: false
    }

    toggleEditMode = () => {
        this.setState({
            editing: !this.state.editing
        })
    }

    updateValue = () => {
        this.setState({
            editing: false,
            value: this.refs.textInput.value
        })
    }

    renderEditView = () => {
        return <div>
            <input
                type="text"
                defaultValue={this.state.value}
                ref="textInput"
            />
            <button onClick={this.toggleEditMode}>Cancel</button>
            <button onClick={this.updateValue}>OK</button>
        </div>
    }

    renderDefaultView = () => {
        return <div onDoubleClick={this.toggleEditMode}>{ this.state.value }</div>
    }

    render() {
        return this.state.editing ? this.renderEditView() : this.renderDefaultView()
    }
}

export default EditableField
