import React from 'react';
import './App.css';
import EditableField from './EditableField';

function App() {
  return (
    <div className="App">
      <EditableField></EditableField>
    </div>
  );
}

export default App;
